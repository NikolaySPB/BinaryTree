﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            var tree = new Tree<int>();
            tree.Add(8);
            tree.Add(5);
            tree.Add(2);
            tree.Add(6);        
            tree.Add(12);      
            tree.Add(15);
            tree.Add(24);

            foreach (var item in tree.Preorder())
            {
                Console.Write(item + ", ");
            }

            Console.WriteLine();

            foreach (var item in tree.Postorder())
            {
                Console.Write(item + ", ");
            }

            Console.WriteLine();

            foreach (var item in tree.Inorder())
            {
                Console.Write(item + ", ");
            }

            Console.ReadLine();
        }
    }
}
