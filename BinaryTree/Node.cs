﻿using System;

namespace BinaryTree
{
    class Node<T> : IComparable<T>, IComparable
        where T : IComparable, IComparable<T>
    {
        public Node<T> Left { get; private set; }
        public Node<T> Right { get; private set; }
        public T Value { get; private set; }

        public Node(T value)
        {
            Value = value;
        }

        public Node(T value, Node<T> left, Node<T> right)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public void Add(T value)
        {
            var node = new Node<T>(value);

            if (node.Value.CompareTo(Value) == -1)
            {
                if (Left == null)
                {
                    Left = new Node<T>(value);
                }
                else
                {
                    Left.Add(value);
                }
            }
            else
            {
                if (Right == null)
                {
                    Right = node;
                }
                else
                {
                    Right.Add(value);
                }
            }
        }

        public int CompareTo(object obj)
        {
            if (obj is Node<T> item)
            {
                return Value.CompareTo(item);
            }
            else
            {
                throw new ArgumentException("Не совпадение типов");
            }
        }

        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
